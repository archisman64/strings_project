function splitTheIP(string) {
    const splitContents = string.split('.');
    if(splitContents.length !== 4) {
        return [];
    }
    // function to check validity of each octet in IPv4
    function isValidOctet(octet) {
        const isNumeric = !isNaN(octet);
        let doesNotHaveLeadingZeroes = octet[0] != 0;
        const isValidInteger = octet >= 0 && octet <=255;
        // handling the exception that a single '0' is a valid octet
        if(octet.length === 1 &&  octet == 0) {
            doesNotHaveLeadingZeroes = true;
        }
        // console.log(octet, isNumeric, doesNotHaveLeadingZeroes, isValidInteger);
        // console.log(isNumeric && doesNotHaveLeadingZeroes && isValidInteger);
        return isNumeric && doesNotHaveLeadingZeroes && isValidInteger;
    }
    // iterating through each octet and checking any anomaly
    for(let index = 0; index < splitContents.length; index++){
        if(!isValidOctet(splitContents[index])){
            console.log('found anomanly');
            return [];
        }
    }
    return splitContents;
}

module.exports = splitTheIP;