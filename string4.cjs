function getFullName(obj) {
    if(typeof obj !== 'object') {
        console.log('input type is invalid');
        return [];
    }
    if(!obj.first_name || !obj.last_name) {
        console.log('improper feilds in the object');
        return [];
    }
    if((obj.first_name && !obj.first_name.trim()) || (obj.last_name && !obj.last_name.trim())) {
        console.log('one or more name feilds are empty');
        return [];
    }
    // function to check if the names has non alphabetic characters
    function hasNonAlphabeticChars(string) {
        return /[^a-zA-Z]/.test(string);
    }
    const firstName = obj.first_name.trim();
    const lastName = obj.last_name.trim();
    let middleName;
    if(obj.middle_name) {
        middleName = obj.middle_name.trim();
    }
    // checking the validity of name strings 
    if(hasNonAlphabeticChars(firstName) || hasNonAlphabeticChars(lastName) || (middleName && hasNonAlphabeticChars(middleName))){
        console.log('Invalid names are present in the object');
        return [];
    }
    // names are valid, now go ahead
    let result = "";
    // concatenating first name, middle name and lastname by following title case.
    result += firstName.charAt(0).toUpperCase() + firstName.slice(1).toLowerCase();
    if(middleName) {
        result += ' ' + middleName.charAt(0).toUpperCase() + middleName.slice(1).toLowerCase();
    }
    result += ' ' + lastName.charAt(0).toUpperCase() + lastName.slice(1).toLowerCase();

    return result;
}

module.exports = getFullName;