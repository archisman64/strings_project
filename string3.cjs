function fetchMonth(dateString) {
    if(typeof dateString !== 'string') {
        console.log('Please supply a date string');
        return [];
    }
    let dateArray = [];
    if(dateString.includes('/')) {
        dateArray = dateString.split('/');
    }
    if(dateArray.length !== 3) {
        console.log('Unsupported format');
        // it is not a valid date in this case
        return [];
    }
    // function to check if date, month and year are valid
    function isValidDate(dateArray) {
        const isNumeric = !isNaN(dateArray[0]) && !isNaN(dateArray[1]) && !isNaN(dateArray[2]);
        const isValidDecimal = (dateArray[0] <=31 && dateArray[0] >= 1) && (dateArray[1] <=12 && dateArray[1] >= 1) && (dateArray[2].length === 4);
        const doesNotHaveLeadingZeroes = dateArray[0][0] != 0 && dateArray[1][0] != 0 && dateArray[2][0] != 0;
        // console.log(isNumeric, isValidDecimal, doesNotHaveLeadingZeroes);
        // console.log(dateArray[0], dateArray[0] <=31 && dateArray[0] >= 1)
        return isNumeric && isValidDecimal && doesNotHaveLeadingZeroes;
    }
    if(isValidDate(dateArray)){
        const monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // return the second element of date array
        return monthArray[parseInt(dateArray[1])-1];
    } else {
        console.log('invalid/unsupprted date format');
        return [];
    }
}

module.exports = fetchMonth;