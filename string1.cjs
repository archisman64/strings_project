function findNumeric(string) {
    function isInvalid(string) {
        // function to check if there are more than one period
        function containMultiplePeriods() {
            let periodCount = 0;
            for(let char of string) {
                if(char === '.') {
                    periodCount++;
                }
            }
            return periodCount > 1;
        }
        // checking if there is any typo, i.e., any unnecessary characters
        const charsToCheck = [];
        for (let index = 0; index <= 255; index++) {
            const char = String.fromCharCode(index);
            if (!/[-$.,0-9]/.test(char)) {
            charsToCheck.push(char);
            }
        }
        const multiplePeriodsExist = containMultiplePeriods(); 
        const includesAnyExtraChar = charsToCheck.some(char => string.includes(char));
        // console.log(multiplePeriodsExist, includesAnyExtraChar);
        // returns true only if their is no typo and only one period
        return multiplePeriodsExist || includesAnyExtraChar;
    }
    if(isInvalid(string)) {
        return 0;
    } else {
        return string.replace(/[$,]/g, "");
    }
}

module.exports = findNumeric;